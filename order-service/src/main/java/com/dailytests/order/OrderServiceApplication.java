package com.dailytests.order;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class OrderServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
//		 SpringApplication app = new SpringApplication(OrderServiceApplication.class);
//	        app.setDefaultProperties(Collections.singletonMap("server.port", "9002"));
//	        app.run(args);
	}
	@Bean
	public RestTemplate restTemplate () {
		return new RestTemplate();
	}

}
