package com.dailytests.order.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Product {
	
	private long tbp_id;
	private String tbp_name;
	private double tbp_price;
	private String tbp_weight;
	private String tbp_status;
	
	public Product() {
		super();
	}
	public Product(long tbp_id, String tbp_name, double tbp_price, String tbp_weight, String tbp_status) {
		super();
		this.tbp_id = tbp_id;
		this.tbp_name = tbp_name;
		this.tbp_price = tbp_price;
		this.tbp_weight = tbp_weight;
		this.tbp_status = tbp_status;
	}
	public long getTbp_id() {
		return tbp_id;
	}
	public void setTbp_id(long tbp_id) {
		this.tbp_id = tbp_id;
	}
	public String getTbp_name() {
		return tbp_name;
	}
	public void setTbp_name(String tbp_name) {
		this.tbp_name = tbp_name;
	}
	public double getTbp_price() {
		return tbp_price;
	}
	public void setTbp_price(double tbp_price) {
		this.tbp_price = tbp_price;
	}
	public String getTbp_weight() {
		return tbp_weight;
	}
	public void setTbp_weight(String tbp_weight) {
		this.tbp_weight = tbp_weight;
	}
	public String getTbp_status() {
		return tbp_status;
	}
	public void setTbp_status(String tbp_status) {
		this.tbp_status = tbp_status;
	}
	
	
}
