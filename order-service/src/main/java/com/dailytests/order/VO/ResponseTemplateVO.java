package com.dailytests.order.VO;

import java.util.Optional;

import com.dailytests.order.entity.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplateVO {

	private Order order;
	private Product product;
	
	
	public ResponseTemplateVO() {
		super();
	}
	public ResponseTemplateVO(Order order, Product product) {
		super();
		this.order = order;
		this.product = product;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setOrder(Optional<Order> order2) {
		// TODO Auto-generated method stub
		
	}
	
	
}
