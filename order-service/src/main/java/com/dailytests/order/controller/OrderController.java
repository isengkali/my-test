package com.dailytests.order.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dailytests.order.VO.ResponseTemplateVO;
import com.dailytests.order.entity.Order;
import com.dailytests.order.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@PostMapping("/saveOrder")
	public Order saveOrder(@RequestBody Order order) {
		return orderService.saveOrder(order);
	}
	
//	@GetMapping("/find{id}")
//	public Optional<Order> findOrderbyId (@PathVariable("id") long order_id) {
//		return service.findbyOrderId(order_id);
//	}
	
	@GetMapping("/{id}")
	public ResponseTemplateVO getOrderWithProduct(@PathVariable("id") Long order_id) {
		return orderService.getOrderWithProduct(order_id);
	}
}
