package com.dailytests.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_order")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long order_id;
	
	@Column(name = "order_name")
	private String order_name;
	
	@Column(name = "order_status")
	private String order_status;
	
	@Column(name = "tbp_id")
	private long tbp_id;

	public Order() {
		super();
	}

	public Order(String order_name, String order_status, long tbp_id) {
		super();
		this.order_name = order_name;
		this.order_status = order_status;
		this.tbp_id = tbp_id;
	}


	public long getTbp_id() {
		return tbp_id;
	}


	public void setTbp_id(long tbp_id) {
		this.tbp_id = tbp_id;
	}


	public long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(long order_id) {
		this.order_id = order_id;
	}

	public String getOrder_name() {
		return order_name;
	}

	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	
	
	
}
