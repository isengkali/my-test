package com.dailytests.order.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dailytests.order.VO.Product;
import com.dailytests.order.VO.ResponseTemplateVO;
import com.dailytests.order.entity.Order;
import com.dailytests.order.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private RestTemplate restTemplate;

	public Order saveOrder(Order order) {
		return orderRepository.save(order);
	}

	public Optional<Order> findbyOrderId(long order_id) {
		return orderRepository.findById(order_id);
	}

	public ResponseTemplateVO getOrderWithProduct(Long order_id) {
		ResponseTemplateVO vo = new ResponseTemplateVO();
		Optional<Order> order = orderRepository.findById(order_id); 
		Product product = restTemplate.getForObject("http://localhost:9001/product/find"+order.get().getTbp_id(), Product.class);
		vo.setOrder(order.get());
		vo.setProduct(product);
		return vo;
	}

	
}
